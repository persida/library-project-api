package com.persidacvetkoska.libraryprojectapi.author;

import com.persidacvetkoska.libraryprojectapi.exception.ResourceAlreadyExistsException;
import com.persidacvetkoska.libraryprojectapi.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthorService {

    private AuthorRepository authorRepository;

    public AuthorDto createAuthor(AuthorCreateRequest request){
        if(authorRepository.findByNameAndDateOfBirth(request.name, request.dateOfBirth).isPresent()){
            throw new ResourceAlreadyExistsException("Author already exists!");
        }
        return authorRepository.save(request.toEntity()).toDto();
    }

    public AuthorDto findByName(String authorName) {
        return this.authorRepository.findByNameIgnoreCase(authorName)
                .orElseThrow(() -> new ResourceNotFoundException("Author not found")).toDto();
    }
}
