package com.persidacvetkoska.libraryprojectapi.author;

import java.time.Instant;
import java.time.LocalDate;

public class AuthorCreateRequest {
    public String name;
    public LocalDate dateOfBirth;
    public String shortBiography;

    public Author toEntity(){
        return new Author(this.name, this.dateOfBirth, this.shortBiography);
    }
}
