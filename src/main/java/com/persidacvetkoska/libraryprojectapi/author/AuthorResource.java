package com.persidacvetkoska.libraryprojectapi.author;

import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/authors")
@CrossOrigin("http://localhost:4200")
public class AuthorResource {

    private AuthorService authorService;

    public AuthorResource(AuthorService authorService){
        this.authorService = authorService;
    }

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public AuthorDto createAuthor(@RequestBody AuthorCreateRequest request) {
        return authorService.createAuthor(request);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public AuthorDto findAuthorByName(@RequestParam String author){
        return this.authorService.findByName(author);
    }

}
