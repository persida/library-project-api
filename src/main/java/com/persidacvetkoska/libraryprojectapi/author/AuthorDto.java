package com.persidacvetkoska.libraryprojectapi.author;

import lombok.AllArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
public class AuthorDto {

    public Long id;
    public String name;
    public LocalDate dateOfBirth;
    public String shortBiography;
}
