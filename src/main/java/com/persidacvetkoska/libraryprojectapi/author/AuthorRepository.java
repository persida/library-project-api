package com.persidacvetkoska.libraryprojectapi.author;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Long> {

    Optional<Author> findByNameAndDateOfBirth(String name, LocalDate dateOfBirth);

    Optional<Author> findByNameIgnoreCase(String name);
}
