package com.persidacvetkoska.libraryprojectapi.author;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "authors")
@NoArgsConstructor
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_generator")
    @SequenceGenerator(name = "author_generator", sequenceName = "authors_id_seq", allocationSize=1)
    public Long id;
    public String name;
    public LocalDate dateOfBirth;
    public String shortBiography;

    public Author(String name, LocalDate dateOfBirth, String shortBiography) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.shortBiography = shortBiography;
    }

    public AuthorDto toDto(){
        return new AuthorDto(this.id, this.name, this.dateOfBirth, this.shortBiography);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(id, author.id) && Objects.equals(name, author.name) && Objects.equals(dateOfBirth, author.dateOfBirth) && Objects.equals(shortBiography, author.shortBiography);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
