package com.persidacvetkoska.libraryprojectapi.book;

import com.persidacvetkoska.libraryprojectapi.author.Author;
import com.persidacvetkoska.libraryprojectapi.book.category.Category;

public class BookCreateRequest {

    public String title;
    public String description;
    public Double addCost;
    public String format;
    public Integer categoryId;
    public Integer numPages;
    public Integer duration;
    public String imagePath;
    public Long authorId;

    public Book toEntity(Author author, Category category){
        return new Book(this.title, this.description, this.addCost, this.format, category, this.numPages,
                this.duration, this.imagePath, author);
    }
}
