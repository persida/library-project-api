package com.persidacvetkoska.libraryprojectapi.book;

import org.springframework.data.jpa.domain.Specification;

public class BookSpecification {

    public static Specification<Book> bySearchPhrase(final String searchPhrase){
        var searchPhraseToLower = '%' + searchPhrase.toLowerCase() + '%';
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.or(
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("title")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("author").get("name")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("category").get("category")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("format")), searchPhraseToLower)
                );
    }
}
