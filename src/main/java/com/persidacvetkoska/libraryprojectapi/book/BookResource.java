package com.persidacvetkoska.libraryprojectapi.book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/books")
@CrossOrigin("http://localhost:4200")
public class BookResource {

    private BookService bookService;

    public BookResource(BookService bookService) { this.bookService = bookService; }

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public BookDto createBook(@RequestBody BookCreateRequest request){
        return this.bookService.createBook(request);
    }
    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<BookDto> findPage(
        @RequestParam(value = "name", required = false) final String title,
        @RequestParam(value = "author", required = false) final String authorName,
        @RequestParam(value = "category", required = false) final String category,
        @RequestParam(value = "format", required = false) final String format,
        Pageable pageable){
            return bookService.findPage(new BookSearchRequest(title, authorName, category, format, pageable));
    }

    @GetMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public BookDto findById(@PathVariable Long id){
        return bookService.findById(id);
    }

}

