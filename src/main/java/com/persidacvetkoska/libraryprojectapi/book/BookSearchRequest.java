package com.persidacvetkoska.libraryprojectapi.book;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

@AllArgsConstructor
public class BookSearchRequest {

    public String title;
    public String authorName;
    public String category;
    public String format;
    public Pageable pageable;

    public Specification<Book> generateSpecification(){
        Specification<Book> bookSpecification = Specification.where(null);

        if (StringUtils.hasText(title)){
            bookSpecification = bookSpecification.and(BookSpecification.bySearchPhrase(title));
        }

        if (StringUtils.hasText(authorName)){
            bookSpecification = bookSpecification.and(BookSpecification.bySearchPhrase(authorName));
        }

        if (StringUtils.hasText(category)){
            bookSpecification = bookSpecification.and(BookSpecification.bySearchPhrase(category));
        }

        if (StringUtils.hasText(format)){
            bookSpecification = bookSpecification.and(BookSpecification.bySearchPhrase(format));
        }

        return bookSpecification;
    }
}
