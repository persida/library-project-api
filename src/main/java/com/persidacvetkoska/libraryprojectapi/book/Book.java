package com.persidacvetkoska.libraryprojectapi.book;

import com.persidacvetkoska.libraryprojectapi.author.Author;
import com.persidacvetkoska.libraryprojectapi.book.category.Category;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "books")
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_generator")
    @SequenceGenerator(name = "book_generator", sequenceName = "books_id_seq", allocationSize=1)
    public Long id;
    public String title;
    public String description;
    @Column(name = "additional_cost")
    public Double addCost;
    public String format;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="category_id")
    public Category category;
    @Column(name="number_of_pages")
    public Integer numPages;
    public Integer duration;
    public String imagePath;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="author_id")
    public Author author;

    public Book(String title, String description, Double addCost, String format, Category category, Integer numPages, Integer duration, String imagePath, Author author) {
        this.title = title;
        this.description = description;
        this.addCost = addCost;
        this.format = format;
        this.category = category;
        this.numPages = numPages;
        this.duration = duration;
        this.imagePath = imagePath;
        this.author = author;
    }

    public BookDto toDto(){
        return new BookDto(this.id, this.title, this.description, this.addCost, this.format, this.category.category,
                this.numPages, this.duration, this.imagePath, this.author.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(id, book.id) && Objects.equals(title, book.title) && Objects.equals(description, book.description) && Objects.equals(addCost, book.addCost) && Objects.equals(format, book.format) && Objects.equals(category, book.category) && Objects.equals(numPages, book.numPages) && Objects.equals(duration, book.duration) && Objects.equals(imagePath, book.imagePath) && Objects.equals(author, book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
