package com.persidacvetkoska.libraryprojectapi.book;

import com.persidacvetkoska.libraryprojectapi.author.Author;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BookDto {
    public Long id;
    public String title;
    public String description;
    public Double addCost;
    public String format;
    public String category;
    public Integer numPages;
    public Integer duration;
    public String imagePath;
    public String authorName;
}
