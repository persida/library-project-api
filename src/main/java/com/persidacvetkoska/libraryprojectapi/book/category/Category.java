package com.persidacvetkoska.libraryprojectapi.book.category;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "categories")
@NoArgsConstructor
public class Category {

    @Id
    public Integer id;
    public String category;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category1 = (Category) o;
        return Objects.equals(id, category1.id) && Objects.equals(category, category1.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
