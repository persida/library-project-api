package com.persidacvetkoska.libraryprojectapi.book;

import com.persidacvetkoska.libraryprojectapi.author.AuthorRepository;
import com.persidacvetkoska.libraryprojectapi.book.category.CategoryRepository;
import com.persidacvetkoska.libraryprojectapi.exception.ResourceAlreadyExistsException;
import com.persidacvetkoska.libraryprojectapi.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BookService {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;
    private CategoryRepository categoryRepository;

    public BookDto createBook(BookCreateRequest request) {
        if(bookRepository.findByTitleAndAuthorIdAndFormat(request.title, request.authorId, request.format).isPresent()){
            throw new ResourceAlreadyExistsException("Book already exists!");
        }
        var author = authorRepository.findById(request.authorId)
                .orElseThrow(() -> new ResourceNotFoundException("Author doesn't exist!"));

        var category = categoryRepository.findById(request.categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category doesn't exist!"));

        return bookRepository.save(request.toEntity(author, category)).toDto();

    }

    public Page<BookDto> findPage(BookSearchRequest request){
        return bookRepository.findAll(request.generateSpecification(), request.pageable)
                .map(book -> book.toDto());
    }

    public BookDto findById(Long id){
        return bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book does not exist!"))
                .toDto();
    }

}
