package com.persidacvetkoska.libraryprojectapi.utils;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ErrorDto {

    public String message;

}