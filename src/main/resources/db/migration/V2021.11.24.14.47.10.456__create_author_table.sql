CREATE TABLE authors (
    id bigserial NOT NULL,
    name character varying(250) NOT NULL,
    date_of_birth timestamp with time zone NOT NULL,
    CONSTRAINT pk_author_id PRIMARY KEY (id)
);
