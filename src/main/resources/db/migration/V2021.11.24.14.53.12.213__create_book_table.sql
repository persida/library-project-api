CREATE TABLE books (
    id bigserial NOT NULL,
    title character varying(250) NOT NULL,
    description character varying(1000) NOT NULL,
    additional_cost numeric(8, 2) NOT NULL,
    format character varying(250) NOT NULL,
    category_id int,
    number_of_pages int,
    duration int,
    image_path varchar,
    author_id bigint,
    CONSTRAINT pk_book_id PRIMARY KEY (id),
    CONSTRAINT fk_author_id FOREIGN KEY(author_id)
            REFERENCES authors(id)
);
CREATE INDEX idx_books_author_id
ON books(author_id);