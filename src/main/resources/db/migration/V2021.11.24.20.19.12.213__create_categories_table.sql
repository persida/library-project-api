CREATE TABLE categories (
    id int NOT NULL,
    category character varying(250) NOT NULL,
    CONSTRAINT pk_category_id PRIMARY KEY (id)
);

INSERT INTO categories VALUES (1, 'Romance');
INSERT INTO categories VALUES (2, 'Action');
INSERT INTO categories VALUES (3, 'Adventure');
INSERT INTO categories VALUES (4, 'Art');
INSERT INTO categories VALUES (5, 'Biography');
INSERT INTO categories VALUES (6, 'History');
INSERT INTO categories VALUES (7, 'Business');
INSERT INTO categories VALUES (8, 'Young Adult');
INSERT INTO categories VALUES (9, 'Horror');
INSERT INTO categories VALUES (10, 'Science Fiction');
INSERT INTO categories VALUES (11, 'Poetry');
INSERT INTO categories VALUES (12, 'Classics');