ALTER TABLE books
ADD CONSTRAINT fk_books_categories FOREIGN KEY (category_id) REFERENCES categories (id);